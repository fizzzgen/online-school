import re
import logging

from sqlalchemy.orm import Session

import datetime
import cruds
import schemas
from core import settings
from core.db import Base, SessionLocal
from cruds import crud_user
from models import *  # noqa: F401


def pascal_case_to_snake(name):
    name = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", name)
    return re.sub("([a-z0-9])([A-Z])", r"\1_\2", name).lower()


def init_db(db: Session = SessionLocal()) -> None:
    try:
            user_in = schemas.UserCreate(
                full_name="root",
                dob=datetime.datetime.now().date(),
                address="root",
                contact_number="777",
                email=settings.FIRST_SUPERUSER,
                password=settings.FIRST_SUPERUSER_PASSWORD,
                user_type=settings.UserType.SUPERADMIN.value,
            )
            super_user = cruds.crud_user.create(db, obj_in=user_in)  # noqa: F841
    except:
            logging.exception('create superuser')


# def init_permissions(db: Session = SessionLocal()) -> None:
#     super_user = crud_user.get_by_id(db, id=1)
#     for model in Base.__subclasses__():
#         try:
#             name = pascal_case_to_snake(model.__name__)
#             permission_create = schemas.UserPermissionCreate(name=f"{name}_create")
#             permission_create = cruds.crud_user_permission.create(
#                 db,
#                 obj_in=permission_create,
#             )
#         except Exception:  # noqa
#             pass

#         try:
#             name = pascal_case_to_snake(model.__name__)
#             permission_update = schemas.UserPermissionCreate(name=f"{name}_update")
#             permission_update = cruds.crud_user_permission.create(
#                 db,
#                 obj_in=permission_update,
#             )
#         except Exception:  # noqa
#             pass

#         try:
#             name = pascal_case_to_snake(model.__name__)
#             permission_retrieve = schemas.UserPermissionCreate(name=f"{name}_get")
#             permission_retrieve = cruds.crud_user_permission.create(
#                 db,
#                 obj_in=permission_retrieve,
#             )
#         except Exception:  # noqa
#             pass
#         try:
#             name = pascal_case_to_snake(model.__name__)
#             permission_retrieve = schemas.UserPermissionCreate(name=f"{name}_get_self")
#             permission_retrieve = cruds.crud_user_permission.create(
#                 db,
#                 obj_in=permission_retrieve,
#             )
#         except Exception:  # noqa
#             pass

#         try:
#             name = pascal_case_to_snake(model.__name__)
#             permission_retrieve = schemas.UserPermissionCreate(
#                 name=f"{name}_update_self"
#             )
#             permission_retrieve = cruds.crud_user_permission.create(
#                 db,
#                 obj_in=permission_retrieve,
#             )
#         except Exception:  # noqa
#             pass
